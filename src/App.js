import React, { Component } from 'react';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    var cards = [ {"title": "Prospective Jobs", "values": ["Product Designer", "Product Manager"]},
                  {"title": "Wish List", "values": ["Fishing Rod"]},
                  {"title": "Home Improvement", "values": ["Nails", "Hammer", "Tape Measure"]}
    ];
    this.state = {
      cards: cards
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ new_val: event.target.value})
  }

  handleSubmit(index, event) {
    event.preventDefault();
    var new_array = this.state.cards;
    new_array[index]["values"].push(this.state.new_val);
    this.setState({cards: new_array});
    document.getElementById("new_item").reset();
  }

  getInputForm(index) {
    let cardLen = this.state.cards.length;
    if(index !== cardLen - 1) {
      return;
    }
    return (
      <form id="new_item" onSubmit={this.handleSubmit.bind(this, index)}>
        <div class="form-group">
            <input class="form-control input-sm" id="inputsm" type="text" onChange={this.handleChange}/>
        </div>
      </form>
    );
  }

  extractValues(values) {
    return (
      <div>
        {values.map(function(value, index) {
          return (
            <p class="card-text">{value}</p>
          );
        }, this)}
      </div>
    )
  }

  render() {
    return (
      <div className="App">
        <div class="row dashboard-background p-sm-2">
        {this.state.cards.map(function(card, index) {
        return (
            <div class="col p-sm-1">
                <div class="card">
                    <div class="card-body text-center p-sm-2">
                        <h5 class="card-title category category-title p-sm-2">{card["title"]}</h5>
                        {this.getInputForm(index)}
                        {this.extractValues(card["values"])}
                    </div>
                </div>
            </div>
          );
        }, this)}
        </div>
      </div>
    );
  }
}

export default App;
